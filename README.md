# Dissection of affinity captured LINE-1 macromolecular complexes

- data/ -- directory with data files for the analysis
- src/ -- scripts with analysis
- lib/ -- functions and constants
- out/ -- results of analysis

# License

This work is licensed under a GNU GPLv3.